package com.lobotus.user.travelize.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lobotus.user.travelize.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import AppUtility.Utility;
import Pojo.Meetings;

public class DistanceshowActivity extends AppCompatActivity {
    Button btncalculate;
    RequestQueue distancequeue;
    Map<String, String> paramsdistance;
    String meetingId;
    TextView distanceTravelled, timeTaken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distanceshow);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btncalculate = (Button) findViewById(R.id.btncalculate);

        meetingId = getIntent().getStringExtra("meetingid");

        distanceTravelled = (TextView) findViewById(R.id.txtdistance);
        timeTaken = (TextView) findViewById(R.id.txttime);

        new ShowData().execute();

        btncalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),UploadreceiptActivity.class);
                intent.putExtra("meetingid",meetingId);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this,CurrentLocationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        return  true;
    }
   /* private void postDatagetdistance() {
        distancequeue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, Utility.BASE_URL +"/api/user/PostUpdateDistance", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jsonarray;
                if (response != null) {
                    try {
                        System.out.println("dashboardcount" + response.toString());
                        jsonarray = new JSONArray(response);
                        // pDialog.dismiss();
                        *//*for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            meetingslistpojo = new Meetings();
                            if (obj.getString("Success").contains("1")) {
                                meetingslistpojo.setClient(obj.getString("FullName"));
                                meetingslistpojo.setTiming(obj.getString("OnDate"));
                                meetingslistpojo.setStatus(obj.getString("Status"));
                                meetingsarrayList.add(meetingslistpojo);
                                int count =   meetingsarrayList.size();
                                txtcount.setText(String.valueOf(count));
                                btnviewmeetings.setEnabled(true);
                                Log.d("countvalue", String.valueOf(count));
                            } else if(obj.getString("Success").contains("0")){
                                managesuccess("No Meeting Today");
                                btnviewmeetings.setEnabled(false);
                                btnviewmeetings.setText("No Meetings Today");
                                txtcount.setText("no");
                                meetingsarrayList.clear();
                            }
                        }*//*
                    } catch (JSONException e) {
                        e.printStackTrace();
                       // parammeetingcount.clear();
                        //     pDialog.dismiss();
                        Log.d("exception", "exception");
                        //     alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error ["+error+"]");
                //  pDialog.dismiss();
                parammeetingcount.clear();
                alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
                //   managesuccess("No Response please try again");
            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramsdistance = new HashMap<String, String>();
                paramsdistance.put("UserID", userid);
                parammeetingcount.put("Today", "Today");
                return parammeetingcount;
            }
        };
        distancequeue.add(postReq);
    }*/

    class ShowData extends AsyncTask {



        String uri ="http://betaphase.in/travelizedev/api/user/PostUpdateDistance";
        JSONObject object,jsonObject;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(DistanceshowActivity.this);
            dialog.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            URL url = null;
            try {
                object = new JSONObject();
                object.put("MeetingID",meetingId);



            url = new URL(uri);

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
//            con.setReadTimeout(10000);
//            con.setConnectTimeout(15000);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestMethod("POST");
                con.setDoInput(true);
                OutputStream os = con.getOutputStream();
                os.write(object.toString().getBytes("UTF-8"));
                os.close();
                int status = con.getResponseCode();
                // read the response
                InputStream in = new BufferedInputStream(con.getInputStream());
                String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
                jsonObject = new JSONObject(result);
                in.close();
                con.disconnect();
                return jsonObject.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            dialog.dismiss();
            try {
                distanceTravelled.setText(jsonObject.getString("DistanceTravelled"));
            timeTaken.setText(jsonObject.getString("TimeTaken"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
