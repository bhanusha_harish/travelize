package com.lobotus.user.travelize.Activity;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.lobotus.user.travelize.R;

import org.json.JSONException;
import org.json.JSONObject;

import Sharedprefe.SharedPrefsLogin;

public class SplashActivity extends AppCompatActivity {
    SharedPrefsLogin prefslogin;
    String userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        prefslogin = new SharedPrefsLogin(getApplicationContext());
        setTitle("Travelize");
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    JSONObject joj = null;
                    try {
                        String preferencevalue = prefslogin.getValues(Login.LOGIN);
                        joj = new JSONObject(preferencevalue);
                         userid = joj.getString("UserId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (userid != null && userid != "") {
                        Intent itt = new Intent(SplashActivity.this, Dashboard.class);
                        startActivity(itt);
                        finish();
                    } else {

                        Intent i = new Intent(SplashActivity.this, Login.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        }).start();
    }

}
