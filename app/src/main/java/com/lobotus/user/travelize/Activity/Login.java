package com.lobotus.user.travelize.Activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import AppUtility.Utility;
import Sharedprefe.SharedPrefsLogin;
import app.Config;
import service.SensorService;
import util.NotificationUtils;

import com.google.firebase.messaging.FirebaseMessaging;
import com.lobotus.user.travelize.R;


public class Login extends AppCompatActivity {
//     String PROJECT_NUMBER= "861684810398";  ///// travelize
    Button btnloginsubmit;
    EditText edtusername,edtpassword;
    private static String Registrationid = null;
    RequestQueue queue;
    Map<String, String> paramslogin;
    String emailid;
    String pwd;
    private ProgressDialog pDialog;
    private static final String TAG = Login.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SharedPrefsLogin prefslogin;
    SharedPreferences prefs;

    public static String LOGIN = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarlogin);
        setSupportActionBar(toolbar);

        if (Build.BRAND.equalsIgnoreCase("xiaomi")) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            startActivity(intent);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    1);
        }


      //  registergcmaccount();
        prefslogin = new SharedPrefsLogin(getApplicationContext());
        prefs = getApplicationContext().getSharedPreferences("mypreferlogin", Context.MODE_PRIVATE);
        edtusername = (EditText)findViewById(R.id.editTextEmail);
        edtpassword = (EditText)findViewById(R.id.editTextPassword);
        btnloginsubmit = (Button)findViewById(R.id.buttonLogin);
        pDialog = new ProgressDialog(Login.this);
        btnloginsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(Login.this)) {
                    if (Validation()) {
                        displayFirebaseRegId();
                        emailid = edtusername.getText().toString();
                        pwd = edtpassword.getText().toString();
                        if(Registrationid == null){
                            displayFirebaseRegId();
                            return;
                        }
                        try {
                            postData();
                            pDialog.setTitle("Loading...");
                            pDialog.show();
                            pDialog.setCancelable(false);
                            btnloginsubmit.setEnabled(false);
                            btnloginsubmit.setText("Loading...");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Utility.alertMessage(Login.this, "Try Again!", "Network failure");
                }
            }
        });
        edtpassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
               //     managesuccess(message);
            //        handleNotification(message);
                   // NotificationUtils.showNotificationMessage
                  //  txtMessage.setText(message);
                }
            }
        };
        displayFirebaseRegId();
    }
    public Boolean Validation() {
        boolean result = true;
        if (isValidEmaillId(edtusername.getText().toString().trim())) {
        } else {
            edtusername.setError("please enter valid email");
            result = false;
        }
        if (edtpassword.getText().toString().trim().equals("")) {
            edtpassword.setError("Please enter valid password");
            result = false;
        }
        return result;
    }
    private boolean isValidEmaillId(String emailid) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(emailid).matches();
    }
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Registrationid =  regId;
        Log.e(TAG, "Firebaseregid:" + regId);

      //  if (!TextUtils.isEmpty(regId))
          //  txtRegId.setText("Firebase Reg Id: " + regId);
       // else
           // txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;
            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }
    private void postData() {
        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, "http://demo.travelize.in/api/User/PostLogin", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    if (jsonObj.getString("Success").contains("1") == true){
                        pDialog.dismiss();
                        String daysremaining =  jsonObj.getString("DaysLeft");
                        int  days  = Integer.parseInt(daysremaining);
                        if(days <= 0){
                            Intent itt = new Intent(Login.this, ExpiryActivity.class);
                            startActivity(itt);
                            finish();
                        } else {
                            btnloginsubmit.setEnabled(true);
                            paramslogin.clear();
                            String s = jsonObj.toString();
                            prefslogin.putvalues(Login.LOGIN, s);
                            Log.d(TAG, "loginsuccess" + prefslogin.getValues(Login.LOGIN));
                          //  pDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
                            btnloginsubmit.setText("Login");
                           Intent i = new Intent(Login.this, Dashboard.class);
                            startActivity(i);
                            finish();
                        }

                    } else if (jsonObj.getString("Success").contains("0") == true) {
                        Log.d("email", emailid + " " + pwd);
                        String message = jsonObj.getString("Msg");
                        pDialog.dismiss();
                        managesuccess(message);
                        paramslogin.clear();
                        btnloginsubmit.setEnabled(true);
                        btnloginsubmit.setText("Login");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    paramslogin.clear();
                    pDialog.dismiss();
                    //  managesuccess("Server response error please try again");
                    btnloginsubmit.setEnabled(true);
                    btnloginsubmit.setText("Login");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error ["+error+"]");
                pDialog.dismiss();
                paramslogin.clear();
                managesuccess("Server response error please try again");
                btnloginsubmit.setEnabled(true);
                btnloginsubmit.setText("Login");
            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramslogin = new HashMap<String, String>();
                paramslogin.put("Email", emailid);
                paramslogin.put("Password", pwd);
                paramslogin.put("RegistrationID", Registrationid);
                paramslogin.put("Model",Build.MODEL);
                paramslogin.put("DeviceName",Build.MANUFACTURER);
                paramslogin.put("Version", Build.VERSION.RELEASE);
                return paramslogin;
            }
        };
        queue.add(postReq);
    }



    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
      /// register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
      //  NotificationUtils.clearNotifications(getApplicationContext());
    }
    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
  /*  private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
           // LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }*/
  private void managesuccess(String ss) {
      AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
      builder.setTitle(ss);
      //   builder.setMessage("Sucessfully Updated");
      builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int id) {
                      dialog.dismiss();
                  }
              }
      );
      AlertDialog dialog = builder.create();
      dialog.show();
  }


}
