package com.lobotus.user.travelize.Activity;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lobotus.user.travelize.Manifest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyService extends Service implements LocationListener {
    public static final String MY_ACTION = "MY_ACTION";
    PowerManager.WakeLock wakeLock;
    private double fusedLatitude = 0.0;
    private double fusedLongitude = 0.0;
    double lat, longt;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    RequestQueue queue;
    Map<String, String> paramsnew;
    String latitude, longtitude;
   // TelephonyManager telephonyManager;
  //  myPhoneStateListener psListener;
  //  ArrayList<LatLng> mMarkerPoints;
  //  latlong ltlg;
    int latint;
   //  String MY_ACTION = "MY_ACTION";
    private Handler handler = new Handler();
    String currrentlocation, address,city,state,country, sublocality, currrentlocationaddress;
   /* private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            Log.d("batterylevel", String.valueOf(level) + "%");
            //   batteryTxt.setText(String.valueOf(level) + "%");
        }
    };*/
   private static final int REQUEST_CODE_PERMISSION = 2;


    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();

        //  Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
        //   Log.d("lobotus", "logo");
       // PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);
       // wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
        Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
//        latlog = (latlong) getApplicationContext();
        Log.d("lobotus", "logo");
//        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
       // psListener = new myPhoneStateListener();
      ///  telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
      //  telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    //    mMarkerPoints = new ArrayList<LatLng>();
        //  ltlg = (latlong) MyService.this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
       // MyThread myThread = new MyThread();
       // myThread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // For time consuming an long tasks you can launch a new thread here...
        Toast.makeText(this, " Service Started", Toast.LENGTH_LONG).show();
      //  startFusedLocation();
      //  registerRequestUpdate(this);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onLocationChanged(Location location) {

        //   setFusedLatitude(location.getLatitude());
        //   setFusedLongitude(location.getLongitude());
        lat = location.getLatitude();
        longt = location.getLongitude();
        // onlatlongSelected()
        if (lat != 0.0 && longt != 0.0) {
            latitude = String.valueOf(lat);
            longtitude = String.valueOf(longt);
            Log.d("latitudelongtitude",  " " + latitude + longtitude);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(MyService.this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                address = addresses.get(0).getAddressLine(1);
                sublocality = addresses.get(0).getAddressLine(0);
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                //   txtcurrentlocation.setText(sublocality  + address + city + state + country);
                currrentlocationaddress =  sublocality + ","+ address +","+  city +","+ state +","+ country;
                Log.d("address",  " " + currrentlocationaddress);
            } catch (IOException e) {
                e.printStackTrace();

            }
        } else  {
            Toast.makeText(getApplicationContext(), "null location" + lat + " " + longt, Toast.LENGTH_SHORT).show();
            Log.d("addressnull",  " " + currrentlocationaddress);
        }
       // postData();
        Toast.makeText(getApplicationContext(), "NEW LOCATION RECEIVED" + lat + " " + longt, Toast.LENGTH_SHORT).show();
        Log.d("latlongsiddu", latitude + " " + longtitude);
      //  onProviderDisabled();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
   /*public void startFusedLocation() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }*/

  /*  public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second
        mLocationRequest.setSmallestDisplacement(0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    mGoogleApiClient.connect();
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }*/

   /* private void postData() {
        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, "http://betaphase.in/apitravelize/api/Login/SaveCords", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //    JSONObject jsonObj = new JSONObject(response);
                Log.d("responsesiddu", response.toString());
                Toast.makeText(getApplicationContext(), "Response" + response.toString(), Toast.LENGTH_SHORT).show();
                   *//* if (jsonObj.getString("Success").contains("1") == true && !jsonObj.getString("WidgetID").equals(" ")) {
                        String daysremaining =  jsonObj.getString("DaysLeft");
                        int  days  = Integer.parseInt(daysremaining);


                    } else if (jsonObj.getString("Success").contains("0")) {
                       Log.d("email", emailid + " " + pwd);
                        String message = jsonObj.getString("invalid");
                     //   pDialog.dismiss();
                    //    managesuccess(message);
                        paramsnew.clear();
                    //    buttonRegister.setEnabled(true);
                    //    buttonRegister.setText("Login");
                    }*//*
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error [" + error + "]");
                // pDialog.dismiss();
                paramsnew.clear();
                //  managesuccess("Server response error please try again");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramsnew = new HashMap<String, String>();
                paramsnew.put("Longitude", longtitude);
                paramsnew.put("Latitude", latitude);
                return paramsnew;
            }
        };
        queue.add(postReq);
    }*/

    /*public class myPhoneStateListener extends PhoneStateListener {
        public int signalStrengthValue;

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (signalStrength.isGsm()) {
                if (signalStrength.getGsmSignalStrength() != 99)
                    signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
                else
                    signalStrengthValue = signalStrength.getGsmSignalStrength();
                //  signalStrengthValue = 0;
            } else {
                signalStrengthValue = signalStrength.getCdmaDbm();
            }
            Log.d("signalstrength", String.valueOf(signalStrengthValue));
            // signalTxt.setText("Signal Strength : " + signalStrengthValue);

        }
    }*/

   /* public class MyThread extends Thread {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            final Runnable r = new Runnable()
            {   public void run()
            {
                Intent intent = new Intent();
                intent.setAction(MY_ACTION);
                intent.putExtra("DATAPASSED",currrentlocationaddress);
                sendBroadcast(intent);
//                Log.v("latitude",latitude);
                handler.postDelayed(this, 5000);
            }
            };
            handler.postDelayed(r, 5000);
        }
    }*/

    }
