package com.lobotus.user.travelize.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lobotus.user.travelize.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    Button endDirection;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        endDirection=(Button)findViewById(R.id.endDirection);
        endDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //code ere
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }
}


/*
import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.lobotus.user.travelize.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import AppUtility.Utility;
import Fragments.DataParser;
import Fragments.POJO.Example;
import Fragments.RetrofitMaps;
import Modules.DirectionFinder;
import Modules.DirectionFinderListener;
import Modules.Route;
import Sharedprefe.SharedPrefsLogin;
import Sharedprefe.Sharedstartend;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static com.lobotus.user.travelize.R.id.address;
import static com.lobotus.user.travelize.R.id.map;
import static com.lobotus.user.travelize.R.id.view;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, DirectionFinderListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    String distance = "";
    String duration = "";
    LatLng position;
    TextView ShowDistanceDuration;
    Button btnstartdirection;
    Polyline line;
    LatLng origin, dest;
    String originlattidu,originlongtitude;
    RequestQueue startqueue;
    RequestQueue endqueue;
    Map<String, String> paramsstart;
    Map<String, String> paramsend;
    SharedPrefsLogin prefslogin;
    String userid;
    String addressreceived;
    LatLng p1 = null;
    double lat ,longt;
    String locationAddress;
    String currentlocationaddress;
    Double doublelocationaddresslat;
    Double doublelocationaddresslongt;
    ////////////////////////////////////////////////////////////////////////
    /// shared preferences maps activity
    Sharedstartend prefstartend;
    SharedPreferences prefsstar;
    public static String STARTENDDIRECTION = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        prefslogin = new SharedPrefsLogin(getApplicationContext());
        prefstartend =   new Sharedstartend(getApplicationContext());
        prefsstar = getApplicationContext().getSharedPreferences("mystartenddirection", Context.MODE_PRIVATE);
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
        // getLocationFromAddress(MapsActivity.this, address);
     //   setSupportActionBar(toolbar);
     //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      // getSupportActionBar().setDisplayShowHomeEnabled(true);
     //    fab = (FloatingActionButton) findViewById(R.id.fab);
        // ShowDistanceDuration = (TextView) findViewById(R.id.show_distance_time);
       //  btnDriving = (Button) findViewById(R.id.btnDriving);
        btnstartdirection = (Button) findViewById(R.id.startdirection);
        if(prefstartend.getValues(MapsActivity.STARTENDDIRECTION).contains("1")){
            btnstartdirection.setText("END DIRECTION");
        }  else if(prefstartend.getValues(MapsActivity.STARTENDDIRECTION).contains("0")){
            btnstartdirection.setText("START DIRECTION");
        }
        JSONObject joj = null;
        try {
            String preferencevalue = prefslogin.getValues(Login.LOGIN);
            joj = new JSONObject(preferencevalue);
            userid = joj.getString("UserId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intentreceive = getIntent();
        addressreceived = intentreceive.getStringExtra("address");
       // Geocoder geocoder = null;
       // try {
        //     geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
        //    Log.d("geocoderaddre", String.valueOf(geocoder));
         //   List<Address> addressList = null;
          //  addressList = geocoder.getFromLocationName("5thMainRMVSecondStage,Karnataka560094", 1);
        //    addressList = geocoder.getFromLocationName("No.5, Sri Anjaneya Building, First Floor,, Loop Lane of Race Course Road, Racecourse, Gandhi Nagar, High Grounds, Sampangi Rama Nagar, Bengaluru, Karnataka 560009", 1);
          //  Log.d("addressListlobotus", String.valueOf(addressList));
          //  if (addressList != null)  {
           //     Address address = addressList.get(0);
           //     StringBuilder sb = new StringBuilder();
             //   Log.d("locationAddressnotnull", String.valueOf(address));
              //  sb.append(address.getLatitude()).append(",");
              //  sb.append(address.getLongitude()).append("");
               // result = sb.toString();
          //  } else {
         //       Log.d("locationAddressnull", "null");
         //   }
      //  }  catch (Exception e) {
     //       e.printStackTrace();
      //      Log.d("entertry",  String.valueOf(e.toString()));
     //   }
     //   String result = null;
     //   try {
         //   Log.d("entertry", "");
          */
/*  List<Address> addressList = null;
            try {
                addressList = geocoder.getFromLocationName("10, 5th Main, RMV Second Stage, Next to Lenskart New BEL Road Bangalore, Karnataka 560094", 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    StringBuilder sb = new StringBuilder();
                    Log.d("locationAddressnotnull", String.valueOf(address));
                    sb.append(address.getLatitude()).append(",");
                    sb.append(address.getLongitude()).append("");
                    result = sb.toString();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*//*


      //  GeocodingLocation locationAddress = new GeocodingLocation();
       // locationAddress.getAddressFromLocation(address,
            //    getApplicationContext(), new GeocoderHandler());
       */
/* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            checkLocationPermission();
        }*//*

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        registerRequestUpdate(this);
       */
/* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+origin.latitude+","+ origin.longitude+"&daddr="+12.9295+","+77.6202));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_LAUNCHER );
                //intent.setClassName("com.androidtutorialpoint.googlemapsapp.MapsActivity", "com.androidtutorialpoint.googlemapsapp.MapsActivity");
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });*//*

    }
    public void startenddirectionbtn(View v) {
        if(v.getId() == startdirection){
            if (Utility.isNetworkAvailable(MapsActivity.this)) {
                if (prefstartend.getValues(MapsActivity.STARTENDDIRECTION).contains("1")) {
                    postDataEnd();
                } else if (prefstartend.getValues(MapsActivity.STARTENDDIRECTION).contains("0")) {
                    postDataStart();
                } else if (prefstartend.getValues(MapsActivity.STARTENDDIRECTION).isEmpty()) {
                    //  btnstartdirection.setText("START DIRECTION");
                    postDataStart();
                }
            } else {
                Utility.alertMessage(MapsActivity.this, "Try Again!", "Network failure");
            }
        }
    }
  */
/*  @Override
    public boolean stopService(Intent name) {
        //stopService(intent);
        stopService(new Intent(this, MyService.class));
        return super.stopService(name);
    }*//*

    private void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second
        mLocationRequest.setSmallestDisplacement(100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    //  if (!isGoogleApiClientConnected()) {
                    mGoogleApiClient.connect();
                    //  }
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        if((location.getLatitude() == 0.0) && (location.getLongitude() == 0.0)){

        } else {
            originlattidu = String.valueOf(location.getLatitude());
            originlongtitude = String.valueOf(location.getLongitude());
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
         origin = new LatLng(location.getLatitude(), location.getLongitude());

     //   if (lat != 0.0 && longt != 0.0) {
         //   latitude = String.valueOf(lat);
          //  longtitude = String.valueOf(longt);
          //  Log.d("latitudelongtitude",  " " + latitude + longtitude);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                Log.d("addresses", String.valueOf(addresses));
                currentlocationaddress = addresses.get(0).getAddressLine(1);
                if(currentlocationaddress.isEmpty()){

                } else {
                    try {
                        new DirectionFinder((DirectionFinderListener) MapsActivity.this, currentlocationaddress, addressreceived).execute();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

              //  sublocality = addresses.get(0).getAddressLine(0);
              //  city = addresses.get(0).getLocality();
              //  state = addresses.get(0).getAdminArea();
              //  country = addresses.get(0).getCountryName();
                //   txtcurrentlocation.setText(sublocality  + address + city + state + country);
               // currrentlocationaddress =  sublocality + ","+ address +","+  city +","+ state +","+ country;
                Log.d("currentaddress",  " " + currentlocationaddress);
            } catch (IOException e) {
                e.printStackTrace();

            }
       // } else  {
       //     Toast.makeText(getApplicationContext(), "null location" + lat + " " + longt, Toast.LENGTH_SHORT).show();
       //     Log.d("addressnull",  " " + address);
    //    }
       */
/* if(doublelocationaddresslat!= 0.0 && doublelocationaddresslongt!= 0.0){
           // managesuccess("Unable to get Latitude and Longitude for this address location");
            dest = new LatLng(00.00, 00.00);
        } else if(doublelocationaddresslat == 00.1 && doublelocationaddresslongt == 00.1){
            dest = new LatLng(00.00, 00.00);
            managesuccess("Unable to get Latitude and Longitude for this address location");
        }
        else {
            dest = new LatLng(doublelocationaddresslat, doublelocationaddresslongt);
        }
        if(originlattidu.isEmpty() && originlongtitude.isEmpty()){

        }else {
           // build_retrofit_and_get_response("driving");
        }*//*


    //     getDirectionsUrl(origin, dest);
// Getting URL to the Google Directions API
  //  String url = getDirectionsUrl(origin, dest);
     //   DownloadTask downloadTask = new DownloadTask();
// Start downloading json data from Google Directions API
     //   downloadTask.execute(url);
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void build_retrofit_and_get_response(String type) {
        String url = "https://maps.googleapis.com/maps/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitMaps service = retrofit.create(RetrofitMaps.class);
        Call<Example> call = service.getDistanceDuration("metric", origin.latitude + "," + origin.longitude, doublelocationaddresslat + "," + doublelocationaddresslongt, type);
        //  Call<Example> call = service.getDistanceDuration("metric", "12.9842" + "," + "77.5865", "12.9295" +"," + "77.6202", type);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Response<Example> response, Retrofit retrofit) {
                try {
                    //Remove previous line from map
                    if (line != null) {
                        line.remove();
                    }
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getRoutes().size(); i++) {
                        String distance = response.body().getRoutes().get(i).getLegs().get(i).getDistance().getText();
                        String time = response.body().getRoutes().get(i).getLegs().get(i).getDuration().getText();
                //        ShowDistanceDuration.setText("Distance:" + distance + ", Duration:" + time);
                        String encodedString = response.body().getRoutes().get(0).getOverviewPolyline().getPoints();
                        List<LatLng> list = decodePoly(encodedString);
                        line = mMap.addPolyline(new PolylineOptions()
                                .addAll(list)
                                .width(15)
                                .color(Color.RED)
                                .geodesic(true)
                        );
                    }
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    private void postDataStart() {
            startqueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest postReq = new StringRequest(Request.Method.POST, "http://betaphase.in/travelizedev/api/user/PostUserGetCordinates", new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);
                        if (jsonObj.getString("Success").contains("1") == true) {
                            Log.d("btnstartdirection", "btnstartdirection");
                            prefstartend.putvalues(MapsActivity.STARTENDDIRECTION, "1");
                            Log.d("insertonesuccess", "insertonesuccess" + prefstartend.getValues(MapsActivity.STARTENDDIRECTION));
                         //   btnstartdirection.setEnabled(false);
                         //   btnstartdirection.setText("Disabled");
                            btnstartdirection.setText("END DIRECTION");
                          //  btnenddirection.setEnabled(true);
                            paramsstart.clear();
                        } else if (jsonObj.getString("Success").contains("0") == true) {
                            Log.d("btnenddirection", "btnenddirection");
                            //   Log.d("email", emailid + " " + pwd);
                         //   btnstartdirection.setEnabled(true);
                            paramsstart.clear();
                            String message = jsonObj.getString("Msg");
                            //    pDialog.dismiss();
//                        paramsspinner.clear();
                            //     btnshowdirection.setEnabled(true);
                            //     btnshowdirection.setText("Direction");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        paramsstart.clear();
                        //      pDialog.dismiss();
                        //  managesuccess("Server response error please try again");
                        //     btnloginsubmit.setEnabled(true);
                        //      btnloginsubmit.setText("Login");
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Error [" + error + "]");
                  ///  btnstartdirection.setEnabled(false);
                    paramsstart.clear();
                    //   pDialog.dismiss();
                    //   managesuccess("Server response error please try again");
                    //   btnloginsubmit.setEnabled(true);
                    //   btnloginsubmit.setText("Login");
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    paramsstart = new HashMap<String, String>();
                    paramsstart.put("UserId", userid);
                    paramsstart.put("Latitude", originlattidu);
                    paramsstart.put("Longitude", originlongtitude);
                    paramsstart.put("BatteryStrength", "100%");
                    paramsstart.put("MobileNetwork", "STRONG");
                    paramsstart.put("MeetingID", "NA");
                    paramsstart.put("StartPoint", "YES");
                    paramsstart.put("EndPoint", "No");
                    return paramsstart;
                }
            };
            startqueue.add(postReq);
        }
    private void postDataEnd() {
        endqueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, "http://betaphase.in/travelizedev/api/user/PostUserGetCordinates", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    if (jsonObj.getString("Success").contains("1") == true) {
                        Log.d("btnENDdirection", "btnENDdirection");
                        prefstartend.putvalues(MapsActivity.STARTENDDIRECTION, "0");
                        Log.d("insertZEROsuccess", "insertZEROsuccess" + prefstartend.getValues(MapsActivity.STARTENDDIRECTION));
                        //   btnstartdirection.setEnabled(false);
                        //   btnstartdirection.setText("Disabled");
                        btnstartdirection.setText("START DIRECTION");
                        Intent itt = new Intent(MapsActivity.this, DistanceshowActivity.class);
                        startActivity(itt);
                        finish();
                        //  btnenddirection.setEnabled(true);
                        paramsend.clear();
                    } else if (jsonObj.getString("Success").contains("0") == true) {
                        Log.d("btnenddirection", "btnenddirection");
                        //   Log.d("email", emailid + " " + pwd);
                        //   btnstartdirection.setEnabled(true);
                        paramsend.clear();
                        String message = jsonObj.getString("Msg");
                        //    pDialog.dismiss();
//                        paramsspinner.clear();
                        //     btnshowdirection.setEnabled(true);
                        //     btnshowdirection.setText("Direction");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    paramsend.clear();
                    //      pDialog.dismiss();
                    //  managesuccess("Server response error please try again");
                    //     btnloginsubmit.setEnabled(true);
                    //      btnloginsubmit.setText("Login");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error [" + error + "]");
                ///  btnstartdirection.setEnabled(false);
                paramsend.clear();
                //   pDialog.dismiss();
                //   managesuccess("Server response error please try again");
                //   btnloginsubmit.setEnabled(true);
                //   btnloginsubmit.setText("Login");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramsend = new HashMap<String, String>();
                paramsend.put("UserId", userid);
                paramsend.put("Latitude", originlattidu);
                paramsend.put("Longitude", originlongtitude);
                paramsend.put("BatteryStrength", "100%");
                paramsend.put("MobileNetwork", "STRONG");
                paramsend.put("MeetingID", "NA");
                paramsend.put("StartPoint", "No");
                paramsend.put("EndPoint", "YES");
                return paramsend;
            }
        };
        endqueue.add(postReq);
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled,enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDirectionFinderStart() {

    }

    @Override
    public void onDirectionFinderSuccess(List<Route> route) {

    }

    */
/*  public LatLng getLocationFromAddress(Context context,String strAddress) {

          Geocoder coder = new Geocoder(context);
          List<Address> address;
          LatLng p1 = null;
          try {
              // May throw an IOException
              address = coder.getFromLocationName(strAddress, 5);
              if (address == null) {
                  return null;
              }
              Address location = address.get(0);
              location.getLatitude();
              location.getLongitude();

              p1 = new LatLng(location.getLatitude(), location.getLongitude());
             Log.d("latlongsystosoft",String.valueOf(p1));
          } catch (IOException ex) {

              ex.printStackTrace();
          }

          return p1;
      }*//*

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {

            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    Log.d("locationAddress", locationAddress);
                    break;
                default:
                    locationAddress = null;
            }
            // doublelocationaddress = Double.parseDouble(locationAddress);
            String string = locationAddress;
            String[] parts = string.split(",");
            String part1 = parts[0]; // 004
            String part2 = parts[1]; // 034556

            if(part1.contains("Unable to get Latitude and Longitude for this address location")){
                doublelocationaddresslat = 00.1;
            } else {
                doublelocationaddresslat = Double.parseDouble(part1);
            }
            if(part2.contains("lobotus")){
                doublelocationaddresslongt = 00.1;
            } else {
                doublelocationaddresslongt = Double.parseDouble(part2);
            }

            Log.d("locationAddress", String.valueOf(doublelocationaddresslat + "," + doublelocationaddresslongt));
          //  latLongTV.setText(locationAddress);
        }
    }
    private void managesuccess(String ss) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MapsActivity.this);
       // builder.setTitle(ss);
        builder.setMessage(ss);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    }

*/
