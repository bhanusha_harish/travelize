package com.lobotus.user.travelize.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.lobotus.user.travelize.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Karthik on 22-06-2017.
 */

public class MapsThreeActivity extends FragmentActivity implements OnMapReadyCallback {
    Button findRoute, startNaviagtion;
    private GoogleMap mMap;
    double srclat, srcLong, destLat, destLong;
    String distance;
    Button endDirection;
    Boolean state = false;
    String meetingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_new);
        endDirection = (Button) findViewById(R.id.endDirection);
        endDirection.setText("Start Route");

        meetingId = getIntent().getStringExtra("meetingid");

        endDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(state==false) {
                    endDirection.setText("End Route");
                    new RouteEndStartAsync(state).execute();
                    state = true;
                }
                else {
                    new RouteEndStartAsync(state).execute();
                    state=false;
                }

            }
        });
        srclat = getIntent().getDoubleExtra("sourceLat", 0.0);
        srcLong = getIntent().getDoubleExtra("sourceLong", 0.0);
        destLat = getIntent().getDoubleExtra("destinationLat", 0.0);
        distance = getIntent().getStringExtra("distance");
        destLong = getIntent().getDoubleExtra("destinationLong", 0.0);
        String Url = makeURL(srclat, srcLong, destLat, destLong);
        new connectAsyncTask(Url).execute();
        //startNaviagtion = (Button) findViewById(R.id.start_naviagtion);
//        startNaviagtion.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + srclat + "," + srcLong + "&daddr=" + destLat + "," + destLong));
//                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                startActivity(intent);
//            }
//        });
//        findRoute = (Button) findViewById(R.id.findRoute);
//        findRoute.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //code ere
//                String Url = makeURL(srclat, srcLong, destLat, destLong);
//                new connectAsyncTask(Url).execute();
////                LatLng location = new LatLng(srclat, srcLong);
////                Marker m3 = mMap.addMarker(new MarkerOptions()
////                        .position(location)
////                        .anchor(0.5f, 0.5f)
////                        .title("Title3")
////                        .snippet("Snippet3")
////                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.distance_marker)));
//            }
//        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //Calculate Time
        String completeDistance = distance;
        String caldIstance = completeDistance.replace("km", "");
        int speedIs10MetersPerMinute = 666;
        float total = Float.parseFloat(caldIstance);
        float estimatedDriveTimeInMinutes = (total * 1000) / speedIs10MetersPerMinute;
        int hours = (int) (estimatedDriveTimeInMinutes / 60); //since both are ints, you get an int
        int minutes = (int) (estimatedDriveTimeInMinutes % 60);

        //Source
        LatLng source = new LatLng(srclat, srcLong);
        Marker marker = mMap.addMarker(new MarkerOptions().position(source).title("Distance :" + distance + " Time:" + hours + ":" + minutes));
        marker.showInfoWindow();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(srclat, srcLong), 12.0f));
        //Destination
        LatLng destinantion = new LatLng(destLat, destLong);
        mMap.addMarker(new MarkerOptions().position(destinantion).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destLat, destLong), 12.0f));

    }
    /*Poly Line Activities*/

    //Genrate Url for Polyline
    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyDuGXAKsOW4R5rxPuWMPDoutc8VeqNhcEE");
        return urlString.toString();
    }

    //Async Task to connect the Parser
    private class connectAsyncTask extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        String url;

        connectAsyncTask(String urlPass) {
            url = urlPass;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(MapsThreeActivity.this);
            progressDialog.setMessage("Fetching route, Please wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            JSONParser jParser = new JSONParser();
            String json = jParser.getJSONFromUrl(url);
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.hide();
            if (result != null) {
                drawPath(result);
            }
        }
    }

    //Drawing the Path
    public void drawPath(String result) {

        try {
            //Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(12)
                    .color(Color.parseColor("#05b1fb"))//Google maps blue color
                    .geodesic(true)
            );
           /*
           for(int z = 0; z<list.size()-1;z++){
                LatLng src= list.get(z);
                LatLng dest= list.get(z+1);
                Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude,   dest.longitude))
                .width(2)
                .color(Color.BLUE).geodesic(true));
            }
           */
        } catch (JSONException e) {

        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public class RouteEndStartAsync extends AsyncTask {
        double lat, lng;

        public RouteEndStartAsync(Boolean state) {
//            this.state = state;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                URL url = new URL("http://demo.travelize.in/api/user/PostSaveCordinates");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(8000);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                lat = srclat;
                lng = srcLong;

                JSONObject param = new JSONObject();
                param.put("Longitude", lng);
                param.put("Latitude", lat);
                param.put("UserId", "BGU002");
                param.put("BatteryStrength", "98%");
                param.put("MobileNetwork", "Strong");
                param.put("MeetingID", meetingId);
                if(state==false){
                    param.put("StartPoint", "Yes");
                    param.put("EndPoint", "No");
                }
                else {
                    param.put("StartPoint", "No");
                    param.put("EndPoint", "Yes");

                }

                os.write(param.toString().getBytes("UTF-8"));
                os.close();
                int status = conn.getResponseCode();
                // read the response
                InputStream in = new BufferedInputStream(conn.getInputStream());
                String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
                JSONObject jsonObject = new JSONObject(result);
                Log.i("Success", jsonObject.getString("Success"));
                Log.i("Msg", jsonObject.getString("Msg"));
                in.close();

                conn.disconnect();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (state == false)
            {
                Intent intent = new Intent(MapsThreeActivity.this,DistanceshowActivity.class);
                intent.putExtra("meetingid",meetingId);
                startActivity(intent);
            }

        }
    }

}
