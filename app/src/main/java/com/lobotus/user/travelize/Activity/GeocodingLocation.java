package com.lobotus.user.travelize.Activity;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocodingLocation {

    private static final String TAG = "GeocodingLocation";

    public static void getAddressFromLocation(final String locationAddress,
                                              final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    Log.d("entertry", "");
                    List<Address> addressList = geocoder.getFromLocationName(locationAddress, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        Log.d("locationAddressnotnull", String.valueOf(address.getLatitude()));
                        sb.append(address.getLatitude()).append(",");
                        sb.append(address.getLongitude()).append("");
                        result = sb.toString();
                    }
                } catch (IOException e) {
                    Log.e("exceptiongeocoding", "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                    //  result = "Address: " + locationAddress +
                   //             "\n\nLatitude and Longitude :\n" + result;
                        result = result;
                       // result = result;
                        bundle.putString("address", result);
                        message.setData(bundle);
                    //    Log.i("locationAddressnotnull", result.toString());
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                       /* result = "Address: " + locationAddress +
                                "\n Unable to get Latitude and Longitude for this address location.";*/
                      //  Log.i("locationAddressresult", result.toString());
                        result = "Unable to get Latitude and Longitude for this address location" + "," + "lobotus";
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}