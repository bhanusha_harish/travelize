package com.lobotus.user.travelize.Activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lobotus.user.travelize.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.DashboardItemsAdapter;
import AppUtility.Utility;
import Pojo.ItemObject;
import Pojo.Meetings;
import Sharedprefe.SharedPrefsLogin;
import service.SensorService;

public class Dashboard extends AppCompatActivity {
    Button btnmeetings, btnmeetinginfo, btnviewmeetings;
    private List<Meetings> meetingsarrayList;
    ListView mDrawerList;
    public ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout drawer;
    CharSequence mDrawerTitle;
    CharSequence mTitle;
    public static boolean isLaunch = true;
    RequestQueue queue;
    Meetings meetingslistpojo;
    Map<String, String> parammeetingcount;
    SharedPrefsLogin prefslogin;
    String userid;
    TextView txtcount;

    Intent mServiceIntent;

    private SensorService mSensorService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Dashboard");
        txtcount = (TextView)findViewById(R.id.txtcount);
        queue = Volley.newRequestQueue(Dashboard.this);
        prefslogin = new SharedPrefsLogin(getApplicationContext());
        meetingsarrayList = new ArrayList<>();

        mSensorService = new SensorService(this);
        mServiceIntent = new Intent(this, mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        btnviewmeetings = (Button) findViewById(R.id.btnviewmeetings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        try {
            JSONObject joj = null;
            String preferencevalue = prefslogin.getValues(Login.LOGIN);
            joj = new JSONObject(preferencevalue);
            userid = joj.getString("UserId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mDrawerToggle = new ActionBarDrawerToggle(Dashboard.this, drawer, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        List<ItemObject> listViewItems = new ArrayList<ItemObject>();
        listViewItems.add(new ItemObject("Home", R.drawable.greenhome));
        listViewItems.add(new ItemObject("Meetings", R.drawable.greenmeetings));
        listViewItems.add(new ItemObject("Claims", R.drawable.greenclaims));
        listViewItems.add(new ItemObject("Orders", R.drawable.greenclaims));
        listViewItems.add(new ItemObject("Sales", R.drawable.greenclaims));
        listViewItems.add(new ItemObject("Account", R.drawable.greenaccount));
        listViewItems.add(new ItemObject("Settings", R.drawable.greensettings));
        mDrawerList.setAdapter(new DashboardItemsAdapter(this, listViewItems));
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openActivity(position);
            }
        });
        if(isLaunch){
            isLaunch = false;
            openActivity(0);
        }
       // openActivity(0);
        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
       /* btnmeetings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.isNetworkAvailable(Dashboard.this)) {
                  //  if(Utility.)
                    Intent intent = new Intent(getApplicationContext(), UploadreceiptActivity.class);
                    startActivity(intent);
                } else {
                    Utility.alertMessage(Dashboard.this,"Try again!", "Network Failure");
                }
            }
        });*/
        btnviewmeetings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.isNetworkAvailable(Dashboard.this)) {
                    Intent intent = new Intent(getApplicationContext(), ViewMeetingsActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utility.alertMessage(Dashboard.this,"Try again!", "Network Failure");
                }
            }
        });
        if(Utility.isNetworkAvailable(Dashboard.this)) {
            postData();
        } else {
            Utility.alertMessage(Dashboard.this,"Try again!", "Network Failure");
        }

    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }
    public void openActivity(int position) {
        mDrawerList.setItemChecked(position, true);
        drawer.closeDrawer(mDrawerList);
      //  Fragment fragment = null;
        switch (position) {
            case 0:
                setTitle("DashBoard");
          //      fragment = new ViewMeetingsFragment();
               //     startActivity(new Intent(this, ViewMeetingActivity.class));
               // } else {
              //      AppUtil.alertMessage(UserListActivity.this, "Try Again!", "Network failure");
              //  }
           //     startActivity(new Intent(this, ViewMeetingActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, MeetingsScheduleActivity.class));
                setTitle("Meetings");
                finish();
             //   startActivity(new Intent(this, ViewMeetingActivity.class));
             //   fragment = new Fragment();
                break;
            case 2:
                startActivity(new Intent(this, ClaimsActivity.class));
                setTitle("Claims");
                //    finish();
                //  fragment = new Settings();
                break;
            case 3:
                startActivity(new Intent(this, OrdersActivity.class));
                setTitle("Orders");
                //    finish();
                //  fragment = new Settings();
                break;
            case 4:
                startActivity(new Intent(this, SalesActivity.class));
                setTitle("Sales");
                //    finish();
                //  fragment = new Settings();
                break;
            case 5:
                startActivity(new Intent(this, AccountActivity.class));
                setTitle("Account");
             //   finish();
               // fragment = new Claims();
                break;
            case 6:
                startActivity(new Intent(this, SettingsActivity.class));
                setTitle("Settings");
              //
             //   finish();
              //  fragment = new Account();
                break;

            default:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }
    private void buildAlertMessageNoGps() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    private void postData() {
        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, Utility.BASE_URL +"/api/User/PostMeetingCount", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jsonarray;
                if (response != null) {
                    try {
                        System.out.println("dashboardcount" + response.toString());
                        jsonarray = new JSONArray(response);
                       // pDialog.dismiss();
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            meetingslistpojo = new Meetings();
                            if (obj.getString("Success").contains("1")) {
                                meetingslistpojo.setClient(obj.getString("FullName"));
                                meetingslistpojo.setTiming(obj.getString("OnDate"));
                                meetingslistpojo.setStatus(obj.getString("Status"));
                                meetingsarrayList.add(meetingslistpojo);
                              int count =   meetingsarrayList.size();
                                txtcount.setText(String.valueOf(count));
                                btnviewmeetings.setEnabled(true);
                                Log.d("countvalue", String.valueOf(count));
                            } else if(obj.getString("Success").contains("0")){
                                managesuccess("No Meeting Today");
                                btnviewmeetings.setEnabled(false);
                                btnviewmeetings.setText("No Meetings Today");
                                txtcount.setText("no");
                                meetingsarrayList.clear();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        parammeetingcount.clear();
                   //     pDialog.dismiss();
                        Log.d("exception", "exception");
                   //     alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error ["+error+"]");
              //  pDialog.dismiss();
                parammeetingcount.clear();
                alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
             //   managesuccess("No Response please try again");
            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                parammeetingcount = new HashMap<String, String>();
                parammeetingcount.put("UserID", userid);
                parammeetingcount.put("Today", "Today");
                return parammeetingcount;
            }
        };
        queue.add(postReq);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(Utility.isNetworkAvailable(Dashboard.this)) {
         //   postData();
        } else {
            Utility.alertMessage(Dashboard.this,"Try again!", "Network Failure");
        }
    }
    private void managesuccess(String ss) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Dashboard.this);
        builder.setTitle(ss);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    public  void alertMessageexception(Context context,String title,String Message) {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(Message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(Utility.isNetworkAvailable(Dashboard.this)) {
                      //      postData();
                        } else {
                            Utility.alertMessage(Dashboard.this,"Try again!", "Network Failure");
                        }
                    }
                }).show();
    }
    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        startService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }

}
