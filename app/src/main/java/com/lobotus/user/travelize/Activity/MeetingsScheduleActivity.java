package com.lobotus.user.travelize.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.lobotus.user.travelize.R;

import java.util.ArrayList;
import java.util.List;

import Adapters.MeetingsAdapter;
import Pojo.Meetings;

public class MeetingsScheduleActivity extends AppCompatActivity {
    private List<Meetings> meetingsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MeetingsAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meetings_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.listviewmeetingschedule);
        //  prepareMovieData();
        mAdapter = new MeetingsAdapter(meetingsList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Meetings meeting = new Meetings("Jain Construction", "12/5/17", "5000");
        meetingsList.add(meeting);

        meeting = new Meetings("Pavitra Enterprises", "15/5/17", "7000");
        meetingsList.add(meeting);

        meeting = new Meetings("Apollo Hospital", "16/5/17","10000");
        meetingsList.add(meeting);

        meeting = new Meetings("Sai Builders Pvt Ltd","18/5/17", "12000");
        meetingsList.add(meeting);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                   Intent intent = new Intent(getApplicationContext(),UploadreceiptActivity.class);
                   startActivity(intent);
                //  meetingsList.clear();
                // Meetings movie = meetingsList.get(position);
                // Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
               /* Currentlocation fragment12 = new Currentlocation();
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment12);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
                //meetingsList.clear();
                //  FragmentManager fragmentManager = getActivity().getFragmentManager();
                //  android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //  fragmentTransaction.replace(R.id.content_frame, fragment11);
                //  fragmentTransaction.addToBackStack(null);
                //  fragmentTransaction.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, Dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        return  true;
    }
}
