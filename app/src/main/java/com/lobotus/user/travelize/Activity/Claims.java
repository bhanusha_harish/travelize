package com.lobotus.user.travelize.Activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.user.travelize.R;

import java.util.ArrayList;
import java.util.List;

import Adapters.ClaimsAdapter;
import Pojo.Claimspojo;

/**
 * A simple {@link Fragment} subclass.
 */
public class Claims extends Fragment {

    private List<Claimspojo> claimsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ClaimsAdapter mAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_claims, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listviewclaims);
        //  prepareMovieData();
        mAdapter = new ClaimsAdapter(claimsList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Claimspojo claimlobo = new Claimspojo("January", "Rs 5000", "Claimed");
        claimsList.add(claimlobo);
        claimlobo = new Claimspojo("Febuary", "Rs 6000", "Rejected");
        claimsList.add(claimlobo);
        claimlobo = new Claimspojo("March", "Rs 7000", "Claimed");
        claimsList.add(claimlobo);
        mAdapter.notifyDataSetChanged();
        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               // Meetings movie = meetingsList.get(position);
               // Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
                Fragment fragment = new Currentlocation();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        return  rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Claims");
    }
    /*private void prepareMovieData() {
        Meetings meeting = new Meetings("Siddu", "hsr", "10.25 Am", "Starting");
        meetingsList.add(meeting);

        meeting = new Meetings("Roopa", "Banashankari", "11.30 Am", "Pending");
        meetingsList.add(meeting);

        meeting = new Meetings("Harry", "Madiwala", "2 Pm","Pending");
        meetingsList.add(meeting);

        meeting = new Meetings("Srilakshmi", "Basavanagudi","4 Pm", "Pending");
        meetingsList.add(meeting);

        mAdapter.notifyDataSetChanged();
    }*/

}
