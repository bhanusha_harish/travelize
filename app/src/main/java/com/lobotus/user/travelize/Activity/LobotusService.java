package com.lobotus.user.travelize.Activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class LobotusService extends Service implements LocationListener {
    public static final String MY_ACTION = "MY_ACTION";
    public int counter = 0;
    double lat, lng;
    LocationManager locationManager;
    String totaladdress = "Loading";
    private Handler handler = new Handler();
    public LobotusService(Context applicationContext) {
        super();

    }

    public LobotusService() {
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i("EXIT", "onTaskRemoved");
        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        startTimer();
            MyThread myThread = new MyThread();
            myThread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime = 0;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 5 seconds
        timer.schedule(timerTask, 5000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                if (isNetworkAvailable()) {
                    try {
                     //   URL url = new URL("");
                      //  HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                      //  conn.setConnectTimeout(8000);
                      //  conn.setRequestProperty("Content-Type", "application/json");
                      //  conn.setDoOutput(true);
                       // conn.setDoInput(true);
                      //  conn.setRequestMethod("POST");
                      //  OutputStream os = conn.getOutputStream();
                        Location loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if(loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
                            lat = loc.getLatitude();
                            lng = loc.getLongitude();
                            Log.d("latlong",lat + " " + lng);
                            Geocoder geocoder;
                            List<Address> addresses;
                            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            //   String knownName = addresses.get(0).getFeatureName();
                            //   JSONObject param = new JSONObject();
                            //   param.put("Longitude", lng);
                            //   param.put("Latitude", lat);
                            //   param.put("Location", address + " " + city + " " + state + " " + country + " " + postalCode);
                            Log.i("ADDRESS", address + " " + city + " " + state + " " + country + " " + postalCode);
                            totaladdress = address + " " + city + " " + state + " " + country + " " + postalCode;
                            //   os.write(param.toString().getBytes("UTF-8"));
                            // os.close();
                            //  int status = conn.getResponseCode();
                            // read the response
                            //  InputStream in = new BufferedInputStream(conn.getInputStream());
                            //   String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
                            ///     JSONObject jsonObject = new JSONObject(result);
                            //  Log.i("Success", jsonObject.getString("Success"));
                            //   Log.i("Msg", jsonObject.getString("Msg"));
                            //  in.close();
                            ///    conn.disconnect();
                        }  else {
                            totaladdress = "Unable to get current location";
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                  //  } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    Log.i("in timer", "in timer ++++  " + (counter++));
                }
            }
        };
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
     public class MyThread extends Thread {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            final Runnable r = new Runnable()

                {
                    public void run ()
                    {
                       // if(totaladdress != null) {
                            Intent intent = new Intent();
                            intent.setAction(MY_ACTION);
                            intent.putExtra("DATAPASSED", totaladdress);
                            sendBroadcast(intent);
                      //  }
                        handler.postDelayed(this, 5000);
                    }
                } ;

                handler.postDelayed(r, 5000);
            }
    }

}

