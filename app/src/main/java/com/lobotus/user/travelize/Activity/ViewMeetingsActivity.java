package com.lobotus.user.travelize.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lobotus.user.travelize.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.EmptyAdapter;
import Adapters.MeetingsAdapter;
import Adapters.ViewMeetingsAdapter;
import AppUtility.Utility;
import Pojo.Meetings;
import Pojo.ViewMeetingspojo;
import Sharedprefe.SharedPrefsLogin;

public class ViewMeetingsActivity extends AppCompatActivity {
    private List<ViewMeetingspojo> viewmeetingsarrayList;
    ViewMeetingspojo  viewmeetingspojolist;
     ListView listViewmeeting;
    Map<String, String> paramviewmeetingcount;
    RequestQueue queue;
    SharedPrefsLogin prefslogin;
    String userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_meetings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        listViewmeeting = (ListView) findViewById(R.id.listviewviewmeeting);
        viewmeetingsarrayList  = new ArrayList<>();
        queue = Volley.newRequestQueue(ViewMeetingsActivity.this);
        prefslogin = new SharedPrefsLogin(getApplicationContext());
        try {
            JSONObject joj = null;
            String preferencevalue = prefslogin.getValues(Login.LOGIN);
            joj = new JSONObject(preferencevalue);
            userid = joj.getString("UserId");
        } catch (JSONException e) {
            e.printStackTrace();
        }


       /* recyclerViewmeeting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  Log.i("timingsiddu", "siddu");
                Intent intent = new Intent(getApplicationContext(), CurrentLocationActivity.class);
                startActivity(intent);
            }
        });*/
        listViewmeeting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             //   Toast.makeText(getApplicationContext(),"check network connection", Toast.LENGTH_SHORT).show();
                Log.i("timingsiddu", "siddu");
                   if (Utility.isNetworkAvailable(ViewMeetingsActivity.this)) {
                       if(viewmeetingsarrayList.size() > 0) {
                           String meetingid = viewmeetingsarrayList.get(position).getMeetingID().toString();
                           String clientname = viewmeetingsarrayList.get(position).getFullName().toString();
                           String address = viewmeetingsarrayList.get(position).getLocation().toString();
                           String phonenumber = viewmeetingsarrayList.get(position).getPhone().toString();
                           String timing = viewmeetingsarrayList.get(position).getOnTime().toString();
                           Intent i = new Intent(getApplicationContext(), CurrentLocationActivity.class);
                           //   pDialog.setTitle("Please wait...");
                           if (i != null) {
                               i.putExtra("clientname", clientname);
                               i.putExtra("address", address);
                               i.putExtra("phonenumber", phonenumber);
                               i.putExtra("timing", timing);
                               i.putExtra("meetingid", meetingid);
                               Log.i("clientname", clientname);
                               Log.i("phonenumber", phonenumber);
                               Log.i("timing", timing);
                               Log.i("address", address);
                               startActivity(i);
                               //  finish();
                           }
                       }
                    } else {
                        Utility.alertMessage(ViewMeetingsActivity.this,"Try again!", "Network Failure");
                    }
            }

        });
        if(Utility.isNetworkAvailable(ViewMeetingsActivity.this)) {
            ViewMeetingspostData();
        } else {
            Utility.alertMessage(ViewMeetingsActivity.this,"Try again!", "Network Failure");
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, Dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        return  true;
    }
    private void ViewMeetingspostData() {
        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, Utility.BASE_URL +"/api/User/PostMeetingCount", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jsonarray;
                if (response != null) {
                    try {
                        System.out.println("viewmeetingshistory" + response.toString());
                        jsonarray = new JSONArray(response);
                        // pDialog.dismiss();
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            viewmeetingspojolist = new ViewMeetingspojo();
                            if (obj.getString("Success").contains("1")) {
                                viewmeetingspojolist.setMeetingID(obj.getString("MeetingID"));
                                viewmeetingspojolist.setFullName(obj.getString("FullName"));
                                viewmeetingspojolist.setPhone(obj.getString("Phone"));
                                viewmeetingspojolist.setStatus(obj.getString("Status"));
                                viewmeetingspojolist.setOnDate(obj.getString("OnDate"));
                                viewmeetingspojolist.setOnTime(obj.getString("OnTime"));
                                viewmeetingspojolist.setLocation(obj.getString("Location"));
                                viewmeetingsarrayList.add(viewmeetingspojolist);
                                listViewmeeting.setAdapter(new ViewMeetingsAdapter(getApplicationContext(), viewmeetingsarrayList));
                            } else {
                                paramviewmeetingcount.clear();
                                List<String> listViewItems = new ArrayList<String>();
                                listViewItems.add("No Meeting Today");
                                listViewmeeting.setAdapter(new EmptyAdapter(getApplicationContext(), listViewItems));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        paramviewmeetingcount.clear();
                        managesuccess("No Response please try again");
                        //     pDialog.dismiss();
                        Log.d("exception", "exception");
                       // alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error ["+error+"]");
                //  pDialog.dismiss();
                paramviewmeetingcount.clear();
               // alertMessageexception(Dashboard.this, "No Response", "Click OK to refresh");
                managesuccess("No Response please try again");
            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramviewmeetingcount = new HashMap<String, String>();
                paramviewmeetingcount.put("UserID", userid);
                paramviewmeetingcount.put("Today", "Today");
                return paramviewmeetingcount;
            }
        };
        queue.add(postReq);
    }
    private void managesuccess(String ss) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ViewMeetingsActivity.this);
        builder.setTitle(ss);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}
