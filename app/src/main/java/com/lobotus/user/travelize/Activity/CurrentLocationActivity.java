package com.lobotus.user.travelize.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lobotus.user.travelize.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.GPSTracker;

public class CurrentLocationActivity extends AppCompatActivity {
    Spinner spinmodeoftravel;
    Button btnshowdirection, btncall;
    MyReceiver myReceiver;
    String datareceived = " ";
    String MY_ACTION = "MY_ACTION";
    TextView txtcurrentlocation, txtmeetingaddress,txtmeetingclientname,txtmeetingphonenumber,txttmeetingstime;
    ProgressDialog prgDialog;
    String phonenumber,meetingid,address;
    RequestQueue queue;
    Map<String, String> paramsspinner;
    String spintext;
    GPSTracker gps;
    double latitude, longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);
        getCurrentLocatin();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
        myReceiver = new MyReceiver();
        getApplicationContext().startService(new Intent(getApplicationContext(),LobotusService.class));
        txtcurrentlocation = (TextView) findViewById(R.id.lblTxtCtLocation);
        ////////////////////////////////////////////////////////////////////////////////////////
        txtmeetingclientname = (TextView) findViewById(R.id.txtmeetingclientname);
        txtmeetingaddress = (TextView) findViewById(R.id.txtmeetingaddress);
        txtmeetingphonenumber = (TextView) findViewById(R.id.txtmeetingphonenumber);
        txttmeetingstime = (TextView) findViewById(R.id.txttmeetingstime);
     /////////////////////////////////////////////////////////////////////////////////////////////
        btnshowdirection = (Button) findViewById(R.id.btnshowdirection);
        btncall = (Button) findViewById(R.id.btncallnow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        txtcurrentlocation.setText("Loading");
        ///////////////////////////////////////////////////////////////////////
        Intent intentreceive = getIntent();
        String  clientname = intentreceive.getStringExtra("clientname");
         address = intentreceive.getStringExtra("address");
         phonenumber = intentreceive.getStringExtra("phonenumber");
        String timing = intentreceive.getStringExtra("timing");
         meetingid = intentreceive.getStringExtra("meetingid");
      ///////////////////////////////////////////////////////////////////////////////
        txtmeetingclientname.setText(clientname);
        txtmeetingaddress.setText(address);
        txtmeetingphonenumber.setText(phonenumber);
        txttmeetingstime.setText(timing);
        /////////////////////////////////////////////////////////////
        Log.d("clientname+address", clientname + address);
        List<String> modeofadapterlist = new ArrayList<String>();
        modeofadapterlist.add("Select");
        modeofadapterlist.add("Bike");
        modeofadapterlist.add("Cab");
        modeofadapterlist.add("Bus");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, modeofadapterlist);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinmodeoftravel = (Spinner) findViewById(R.id.modeofTspinner);
        spinmodeoftravel.setAdapter(dataAdapter);
        spinmodeoftravel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                spintext = parent.getSelectedItem().toString();
                Log.d("spintext", spintext);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
//
       btnshowdirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(datareceived!= null) {
                    postDataspinnerupload();
                }
                    else {
                        Log.d("locationsss", "no value");
                        Toast.makeText(getApplicationContext(), "Loading Location", Toast.LENGTH_SHORT).show();

                    }
               /* if(datareceived!= null){
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(intent);
                    Log.d("move", "value");
                } else {
                    Log.d("locationsss", "no value");
                    Toast.makeText(getApplicationContext(), "Loading Location", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        btncall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(phonenumber != null) {
                 //  Intent callIntent = new Intent(Intent.ACTION_CALL);
                   Intent callIntent = new Intent(Intent.ACTION_DIAL);
                   callIntent.setData(Uri.parse("tel:" + phonenumber));
                   if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                           Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                       startActivity(callIntent);
                       return;
                   }
                   startActivity(callIntent);
               }
            }
        });
     //   prgDialog = new ProgressDialog(this);
      //  prgDialog.setMessage("Loading..");
     //   prgDialog.setCancelable(false);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, ViewMeetingsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        return  true;
    }
    //@Override
    public void onStart() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyService.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);
        super.onStart();
    }
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub
            datareceived = arg1.getStringExtra("DATAPASSED");
             if(datareceived != null){
                 txtcurrentlocation.setText(datareceived);
                 Log.d("datapassed", String.valueOf(datareceived));
            //     prgDialog.dismiss();
               } else {
                 txtcurrentlocation.setText("Loading");
               //  Log.d("datapassed", String.valueOf(datareceived));
             //    Utility.alertMessage(CurrentLocationActivity.this, "Turn On Gps Location", "");
              //   Toast.makeText(getApplicationContext(), "null location", Toast.LENGTH_SHORT).show();
             }
            //  }
         //   Log.d("datapassed", String.valueOf(datareceived));
            // Toast.makeText(AndroidLocationActivity.this,
            //    "Triggered by Service!\n"
            //           + "Data passed: " + String.valueOf(datapassed),
            //   Toast.LENGTH_LONG).show();
        }
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled,enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    private void postDataspinnerupload() {
        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest postReq = new StringRequest(Request.Method.POST, "http://betaphase.in/travelizedev/api/user/PostUpdateMOT", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    if (jsonObj.getString("Success").contains("1") == true){
                        Log.d("spinneruploadSuccess", "spinneruploadSuccess");
                        if(spintext.contains("Select")){
                            managesuccess("Select mode of travel");
                        } else {
//                            Intent itt = new Intent(CurrentLocationActivity.this, MapstwoActivity.class);
//                            itt.putExtra("meetingid",meetingid);
//                            itt.putExtra("address",address);
//                            startActivity(itt);
//                            finish();
                            LatLng latLng=getLocationFromAddress(CurrentLocationActivity.this, address);
                            String distance= getDistance(latitude, longitude, latLng.latitude, latLng.longitude);
                            Intent intent=new Intent(CurrentLocationActivity.this,MapsThreeActivity.class);
                            intent.putExtra("meetingid",meetingid);
                            intent.putExtra("sourceLat",latitude);
                            intent.putExtra("sourceLong",longitude);
                            intent.putExtra("destinationLat",latLng.latitude);
                            intent.putExtra("destinationLong",latLng.longitude);
                            intent.putExtra("distance",distance);
                            startActivity(intent);
                        }

                    } else if (jsonObj.getString("Success").contains("0") == true) {
                     //   Log.d("email", emailid + " " + pwd);
                       String message = jsonObj.getString("Msg");
                    //    pDialog.dismiss();
                        managesuccess(message);
                        paramsspinner.clear();
                        btnshowdirection.setEnabled(true);
                        btnshowdirection.setText("Direction");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
             //       paramslogin.clear();
              //      pDialog.dismiss();
                    //  managesuccess("Server response error please try again");
               //     btnloginsubmit.setEnabled(true);
              //      btnloginsubmit.setText("Login");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error ["+error+"]");
             //   pDialog.dismiss();
             //   paramslogin.clear();
             //   managesuccess("Server response error please try again");
             //   btnloginsubmit.setEnabled(true);
             //   btnloginsubmit.setText("Login");
            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                paramsspinner = new HashMap<String, String>();
                paramsspinner.put("MeetingID", meetingid);
                paramsspinner.put("ModeOfTravel", spintext);
                return paramsspinner;
            }
        };
        queue.add(postReq);
    }
    private void managesuccess(String ss) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CurrentLocationActivity.this);
        builder.setTitle(ss);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    //Get Lat Long from Address
    public LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }
    //Get Current location in GPS
    public void getCurrentLocatin() {
        // create class object
        gps = new GPSTracker(CurrentLocationActivity.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            // \n is for new line
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: "
//                    + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            buildAlertMessageNoGps();
        }
    }
    //Get Distance Between 2 Lat long positions
    public String getDistance(final double lat1, final double lon1, final double lat2, final double lon2) {
        final String[] parsedDistance = new String[1];
        final String[] response = new String[1];
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric&mode=driving");
                    final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response[0] = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
                    JSONObject jsonObject = new JSONObject(response[0]);
                    JSONArray array = jsonObject.getJSONArray("routes");
                    JSONObject routes = array.getJSONObject(0);
                    JSONArray legs = routes.getJSONArray("legs");
                    JSONObject steps = legs.getJSONObject(0);
                    JSONObject distance = steps.getJSONObject("distance");
                    parsedDistance[0] = distance.getString("text");
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return parsedDistance[0];
    }

}
