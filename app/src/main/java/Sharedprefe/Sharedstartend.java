package Sharedprefe;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by User-AD on 19-06-2017.
 */

public class Sharedstartend {
    SharedPreferences prefsstartend;
    private Context ctx;
    public Sharedstartend(Context ctx) {
        this.ctx=ctx;
        prefsstartend=ctx.getSharedPreferences("mystartenddirection", ctx.MODE_PRIVATE);
    }
    public void putvalues(String key, String value) {
        SharedPreferences.Editor edit=prefsstartend.edit();
        edit.putString(key, value);
        edit.commit();
    }
    public String deletePrefs(String key){
        prefsstartend.edit().clear().commit();
        return key;
    }

    public String getValues(String key) {
        String result=prefsstartend.getString(key, "");

        return result;
    }
}
