package Sharedprefe;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsLogin {
    SharedPreferences prefs;
    private Context ctx;
    public SharedPrefsLogin(Context ctx) {
        this.ctx=ctx;
        prefs=ctx.getSharedPreferences("mypreferlogin", ctx.MODE_PRIVATE);
    }
    public void putvalues(String key, String value) {
        SharedPreferences.Editor edit=prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }
    public String deletePrefs(String key){
        prefs.edit().clear().commit();
        return key;
    }

    public String getValues(String key) {
        String result=prefs.getString(key, "");

        return result;
    }


}
