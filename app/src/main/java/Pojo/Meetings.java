package Pojo;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Meetings {
    private String client, timing, status;

    public Meetings() {
    }

    public Meetings(String client, String timing, String status) {
        this.client = client;
       // this.location = location;
        this.timing = timing;
        this.status = status;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }
}
