package Pojo;

/**
 * Created by siddu on 14-03-2017.
 */

public class Claimspojo {
    private String month, amount, status;
    public Claimspojo(String month, String amount, String status) {
        this.month = month;
        this.amount = amount;
        this.status = status;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
