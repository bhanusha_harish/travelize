package Pojo;

/**
 * Created by User-AD on 12-06-2017.
 */

public class ViewMeetingspojo {
    private String MeetingID, UserId, FullName,Phone,Status,OnDate,OnTime,Location;


    public ViewMeetingspojo() {
    }

    public ViewMeetingspojo(String MeetingID, String UserId, String FullName, String Phone,
                            String Status, String OnDate, String OnTime, String Location) {
        this.MeetingID = MeetingID;
        this.UserId = UserId;
        this.FullName = FullName;
        this.Phone = Phone;
        this.Status = Status;
        this.OnDate = OnDate;
        this.OnTime = OnTime;
        this.Location = Location;

    }
    public String getMeetingID() {
        return MeetingID;
    }

    public void setMeetingID(String meetingID) {
        MeetingID = meetingID;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getOnDate() {
        return OnDate;
    }

    public void setOnDate(String onDate) {
        OnDate = onDate;
    }

    public String getOnTime() {
        return OnTime;
    }

    public void setOnTime(String onTime) {
        OnTime = onTime;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}
