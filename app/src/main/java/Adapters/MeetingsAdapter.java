package Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lobotus.user.travelize.R;

import java.util.List;

import Pojo.Meetings;

/**
 * Created by User-AD on 06-04-2017.
 */

public class MeetingsAdapter extends RecyclerView.Adapter<MeetingsAdapter.MyViewHolder> {

    private List<Meetings> meetingsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView client, timing, status;

        public MyViewHolder(View view) {
            super(view);
            client = (TextView) view.findViewById(R.id.client);
            //  location = (TextView) view.findViewById(R.id.location);
            timing = (TextView) view.findViewById(R.id.timing);
            status = (TextView) view.findViewById(R.id.status);
        }
    }

    public MeetingsAdapter(List<Meetings> meetingsList) {
        this.meetingsList = meetingsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meetings_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Meetings meeting = meetingsList.get(position);
        holder.client.setText(meeting.getClient());
        //  holder.location.setText(meeting.getLocation());
        holder.timing.setText(meeting.getTiming());
        holder.status.setText(meeting.getStatus());
    }

    @Override
    public int getItemCount() {
        return meetingsList.size();
    }
}
