package Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.lobotus.user.travelize.R;

import java.util.List;

import Pojo.Claimspojo;

public class ClaimsAdapter extends RecyclerView.Adapter<ClaimsAdapter.MyViewHolder> {

    private List<Claimspojo> claimsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView month, amount, status;

        public MyViewHolder(View view) {
            super(view);
            month = (TextView) view.findViewById(R.id.month);
            amount = (TextView) view.findViewById(R.id.amount);
            status = (TextView) view.findViewById(R.id.claimstatus);
        }
    }
    public ClaimsAdapter(List<Claimspojo> claimsList) {
        this.claimsList = claimsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.claims_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Claimspojo claims = claimsList.get(position);
        holder.month.setText(claims.getMonth());
        holder.amount.setText(claims.getAmount());
        holder.status.setText(claims.getStatus());
    }

    @Override
    public int getItemCount() {
        return claimsList.size();
    }
}
