package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lobotus.user.travelize.R;

import java.util.List;

public class EmptyAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater layoutInflater;
    private List<String> adapterdotnetarraylist;

    public EmptyAdapter(Context context, List<String> listViewItems) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        adapterdotnetarraylist = listViewItems;
    }

    @Override
    public int getCount() {
        return adapterdotnetarraylist.size();
    }
    @Override
    public Object getItem(int position) {
        return adapterdotnetarraylist.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        ViewHolder holder = new ViewHolder();
        rowView = layoutInflater.inflate(R.layout.emptyrow_layout, null);
        holder.txtusermessage = (TextView) rowView.findViewById(R.id.txtemptyusername);
        rowView.setTag(holder);
        holder.txtusermessage.setText(adapterdotnetarraylist.get(position));
        return rowView;
    }
    class ViewHolder {
        TextView txtusermessage;
    }
}