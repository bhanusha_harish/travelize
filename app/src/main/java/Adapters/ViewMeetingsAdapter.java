package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lobotus.user.travelize.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import Pojo.Meetings;
import Pojo.ViewMeetingspojo;

/**
 * Created by User-AD on 06-04-2017.
 */

public class ViewMeetingsAdapter extends BaseAdapter {

    private LayoutInflater lInflater;
    //  private List<Meetingspojo> listStorage;
    private List<ViewMeetingspojo> viewmeetingsarrayList1;
    private Context ctx;


    public ViewMeetingsAdapter(Context context, List<ViewMeetingspojo> viewmeetingsarrayList) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewmeetingsarrayList1 = viewmeetingsarrayList;
        this.ctx = context;
    }


    @Override
    public int getCount() {
        return viewmeetingsarrayList1.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if (convertView == null) {
            listViewHolder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.viewmeetings_list_row, null);
            listViewHolder.textfullname = (TextView) convertView.findViewById(R.id.viewfullname);
             listViewHolder.textViewtime = (TextView) convertView.findViewById(R.id.viewtiming);
            listViewHolder.txtviewstatus = (TextView) convertView.findViewById(R.id.viewstatus);
            convertView.setTag(listViewHolder);
        } else {
            listViewHolder = (ViewHolder) convertView.getTag();
        }
        listViewHolder.textfullname.setText(viewmeetingsarrayList1.get(position).getFullName());
        listViewHolder.textViewtime.setText(viewmeetingsarrayList1.get(position).getOnTime());
        listViewHolder.txtviewstatus.setText(viewmeetingsarrayList1.get(position).getStatus());
        return convertView;
    }

   static class ViewHolder {
       TextView textfullname, textViewtime, txtviewstatus;
   }
}
