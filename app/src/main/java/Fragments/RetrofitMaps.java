package Fragments;

import Fragments.POJO.Example;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

import static com.android.volley.Request.Method.GET;

/**
 * Created by navneet on 17/7/16.
 */
public interface RetrofitMaps {
    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */
    @GET("api/directions/json?key=AIzaSyBYigCkr-oB1W81zBU9qAYHL4oc0qSIS68")
    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

}
